package ru.ocrv.test.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Сообщение")
public class Message {

    @ApiModelProperty(notes = "идетнификатор предмета")
    private String idSample;

    @ApiModelProperty(notes = "серийный номер предмета")
    private String numId;

    @ApiModelProperty(notes = "место откуда")
    private String idLocation;

    @ApiModelProperty(notes = "датчик, с которого идет сигнал")
    private String idSignalPar;

    @ApiModelProperty(notes = "данные о состоянии")
    private String idDetected;

    @ApiModelProperty(notes = "вид поломки")
    private String idClassDet;


    public Message() {
    }

    public Message(String idSample, String numId, String idLocation, String idSignalPar, String idDetected, String idClassDet) {
        this.idSample = idSample;
        this.numId = numId;
        this.idLocation = idLocation;
        this.idSignalPar = idSignalPar;
        this.idDetected = idDetected;
        this.idClassDet = idClassDet;
    }

    public String getIdSample() {
        return idSample;
    }

    @JsonProperty("id_sample")
    public void setIdSample(String idSample) {
        this.idSample = idSample;
    }

    public String getNumId() {
        return numId;
    }

    @JsonProperty("num_id")
    public void setNumId(String numId) {
        this.numId = numId;
    }

    public String getIdLocation() {
        return idLocation;
    }

    @JsonProperty("id_location")
    public void setIdLocation(String idLocation) {
        this.idLocation = idLocation;
    }

    public String getIdSignalPar() {
        return idSignalPar;
    }

    @JsonProperty("id_signal_par")
    public void setIdSignalPar(String idSignalPar) {
        this.idSignalPar = idSignalPar;
    }

    public String getIdDetected() {
        return idDetected;
    }

    @JsonProperty("id_detected")
    public void setIdDetected(String idDetected) {
        this.idDetected = idDetected;
    }

    public String getIdClassDet() {
        return idClassDet;
    }

    @JsonProperty("id_class_det")
    public void setIdClassDet(String idClassDet) {
        this.idClassDet = idClassDet;
    }
}