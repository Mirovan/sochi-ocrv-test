package ru.ocrv.test.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Статистика сообщений")
public class MessageStatistic {

    @ApiModelProperty(notes = "Число ошибок")
    private int failedCount;

    @ApiModelProperty(notes = "Число без ошибок")
    private int doneCount;

    public MessageStatistic(int failedCount, int approvedCount) {
        this.failedCount = failedCount;
        this.doneCount = approvedCount;
    }

    public int getFailedCount() {
        return failedCount;
    }

    public void setFailedCount(int failedCount) {
        this.failedCount = failedCount;
    }

    public int getDoneCount() {
        return doneCount;
    }

    public void setDoneCount(int doneCount) {
        this.doneCount = doneCount;
    }

    /**
     * Метод для обновления числа сообщений с ошибками или сообщений без ошибок
     * */
    public void increment(String st) {
        if ("Nan".equals(st))
            failedCount++;
        else if ("None".equals(st))
            doneCount++;
    }
}