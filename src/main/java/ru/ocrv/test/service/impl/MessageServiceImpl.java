package ru.ocrv.test.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.ocrv.test.model.Message;
import ru.ocrv.test.model.MessageStatistic;
import ru.ocrv.test.service.iface.MessageService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("MessageService")
public class MessageServiceImpl implements MessageService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());


    @Override
    public Map<String, MessageStatistic> analize(Message[] messages) {
        Map<String, List<Message>> messageMap = new HashMap<>();            //мапа для сообщений
        Map<String, MessageStatistic> aggregateMap = new HashMap<>();       //мапа для числа ошибок

        //Кладем все сообщения в массив. Ключ - id_location, значение - массив объектов
        for (Message item: messages) {
            List<Message> list = null;
            MessageStatistic messageStatistic = null;

            //если такого id_location нет - создаем пустой массив
            if ( !messageMap.containsKey(item.getIdLocation()) ) {
                list = new ArrayList<>();
                messageStatistic = new MessageStatistic(0, 0);
            } else { //получаем что лежит в мапе по ключу
                list = messageMap.get(item.getIdLocation());
                messageStatistic = aggregateMap.get(item.getIdLocation());
            }
            //добавляем данные в список, обновляем в мапе сообщений
            list.add(item);
            messageMap.put(item.getIdLocation(), list);

            //обновляем в результирующей мапе
            messageStatistic.increment(item.getIdDetected());
            aggregateMap.put(item.getIdLocation(), messageStatistic);
        }

        return aggregateMap;
    }
}
