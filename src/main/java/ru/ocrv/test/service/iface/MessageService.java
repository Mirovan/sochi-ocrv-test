package ru.ocrv.test.service.iface;

import ru.ocrv.test.model.Message;
import ru.ocrv.test.model.MessageStatistic;

import java.util.Map;

public interface MessageService {
    Map<String, MessageStatistic> analize(Message[] messages); //анализ сообщений
}
