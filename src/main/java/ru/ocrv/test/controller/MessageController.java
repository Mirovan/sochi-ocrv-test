package ru.ocrv.test.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.ocrv.test.model.Message;
import ru.ocrv.test.model.MessageStatistic;
import ru.ocrv.test.service.iface.MessageService;

import java.util.*;


@PropertySource("classpath:application.properties")
@RequestMapping("${api-version.url}")
@RestController
@Api(value="message", description="Операции с сообщениями")
public class MessageController {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MessageService messageService;

    /**
     * Принимаем запрос, обрабатываем данные, отдаём ответ
     * */
    @ApiOperation(value = "Добавление данных и вывод анализа")
    @RequestMapping(value="/add/", method = RequestMethod.POST, produces = "application/json")
    public String addMessage(@RequestBody(required = true) Message[] messages) {
        Map<String, MessageStatistic> aggregateMap = messageService.analize(messages);

        ObjectMapper objectMapper = new ObjectMapper();
        ArrayNode arrayNode = objectMapper.createArrayNode();
        for (String key: aggregateMap.keySet()) {
            JsonNode responseNode = objectMapper.createObjectNode();
            ((ObjectNode) responseNode).put("id_location", key);
            ((ObjectNode) responseNode).put("failed", aggregateMap.get(key).getFailedCount());
            ((ObjectNode) responseNode).put("done", aggregateMap.get(key).getDoneCount());
            arrayNode.add(responseNode);
        }

        return arrayNode.toString();
    }

}