package ru.ocrv.test.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Класс для получения из файла конфигурации
 * имя файла захардкожено.
 * Для запуска с параметрами, использовать:
 * ExecStart=/usr/bin/java -jar {{ app.home }}/{{ app_download_url|basename }} --server.port=80 --spring.config.location=file:/etc/{{ app.name }}/application.properties
 * */
public class Property {

    private static final Logger LOGGER = LoggerFactory.getLogger(Property.class);

    private final String propFileName = "application.properties";

    private Properties properties;

    public Properties loadPropertyValues() throws IOException {
        if (properties == null) loadPropertyValuesFromConfigFile();
        return properties;
    }

    private void loadPropertyValuesFromConfigFile() throws IOException {
        InputStream inputStream = null;
        try {
            properties = new Properties();

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                LoggerUtil.writeError(LOGGER, "property file '" + propFileName + "' not found in the classpath");
            }
        } catch (Exception e) {
            LoggerUtil.writeError(LOGGER, e.getMessage());
        } finally {
            inputStream.close();
        }
    }


    public static String getProperty(String propertyName) {
        Property property = new Property();
        Properties properties = null;
        try {
            properties = property.loadPropertyValues();
        } catch (IOException e) {
            LoggerUtil.writeError(LOGGER, e.getMessage());
        }
        return properties.getProperty(propertyName);
    }


}