package ru.ocrv.test.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.ocrv.test.Application;
import ru.ocrv.test.model.Message;
import ru.ocrv.test.service.iface.MessageService;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class MessageControllerTest {

    @Autowired
    private MessageService messageService;

    @Test
    public void addMessage() {
        ObjectMapper objectMapper = new ObjectMapper();

        String json = "[\n" +
                "{\n" +
                "\t\"id_sample\" : \"2-76rtw\",\n" +
                "\t\"num_id\" : \"ffg#er111\",\n" +
                "\t\"id_location\" : \"3211.2334\",\n" +
                "\t\"id_signal_par\" : \"0xcv11cs\",\n" +
                "\t\"id_detected\" : \"None\",\n" +
                "\t\"id_class_det\" : \"req11\"\n" +
                "},\n" +
                "{\n" +
                "\t\"id_sample\" : \"1\",\n" +
                "\t\"num_id\" : \"1\",\n" +
                "\t\"id_location\" : \"3211.2334\",\n" +
                "\t\"id_signal_par\" : \"0xcv11cs\",\n" +
                "\t\"id_detected\" : \"Nan\",\n" +
                "\t\"id_class_det\" : \"поломка 1\"\n" +
                "},\n" +
                "{\n" +
                "\t\"id_sample\" : \"2\",\n" +
                "\t\"num_id\" : \"2\",\n" +
                "\t\"id_location\" : \"Ekaterinburg\",\n" +
                "\t\"id_signal_par\" : \"0xcv11cs\",\n" +
                "\t\"id_detected\" : \"Nan\",\n" +
                "\t\"id_class_det\" : \"поломка 2\"\n" +
                "}\n" +
                "]";
        try {
            Message message = objectMapper.readValue(json, Message.class);

            //assertEquals(messageService.add(message), true);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}